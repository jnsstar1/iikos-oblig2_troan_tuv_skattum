#!/bin/bash

for i in "$@"; do #forløkke gjennom alle prosessene
  dato=$(date "+%Y%m%d-%H:%M:%S") #finner dato og tid
  status=$(cat /proc/"$i"/status) #henter status fra hver prosess
  vmSize=$(echo "$status" | grep VmSize | awk '{ print $2 }') #finner mengde virituelt minne
  privVM=$(echo "$status" | grep -E "Vm[DSE][atx]" | awk '{ print $2 }' | sed ':a;N;$!ba;s/\n/+/g' | bc) #finner vmdata, vmstk og vmexe og plusser
  deltVM=$(echo "$status" | grep VmLib | awk '{ print $2 }') #finner delt virituelt mine
  fysiskM=$(echo "$status" | grep VmRSS | awk '{ print $2 }') #finner fysisk minne
  pageTableM=$(echo "$status" | grep VmPTE | awk '{ print $2 }') #finner minne brukt av page table
  
  filnavn="$i-$dato.meminfo" #lager filnavn
  touch "$filnavn" #lager en tom fil
  {  
    echo "****** Minne info om prosess med PID $i ******" #printer 
    echo "Total bruk av virtuelt minne (VmSize): $vmSize"
    echo "Mengde privat virtuelt minne (VmData+VmStk+VmExe): $privVM"
    echo "Mengde shared virtuelt minne (VmLib): $deltVM"
    echo "Total bruk av fysisk minne (VmRSS): $fysiskM"
    echo "Mengde fysisk minne som benyttes til page table (VmPTE): $pageTableM"
  } >> "$filnavn" #til fila
done
