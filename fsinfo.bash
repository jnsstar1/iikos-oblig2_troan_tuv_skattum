#!/bin/bash

prosFull=$(df -h "$1" | tail -n 1 | awk '{ print $5 }')          #Finner hvor stor del av partisjonen directorien befinner seg på som er full og lagrer i prosFull
antFiler=$(find "$1" -depth -type f | wc -l)                     #Finner antall filer i directorien og lagrer i antfiler
storsteFil=$(find "$1" -depth -type f | tr '\n' '\0' | du -ah --files0-from=- | sort -h | tail -n 1) #Finner pathen til den største filen i directoriet og lagrer i StorsteFil
totStr=$(find "$1" -depth -type f | tr '\n' '\0' | du -a --files0-from=- | awk '{ print $1 }' | sed ':a;N;$!ba;s/\n/+/g' | bc)   #Finner total størrelse i directoriet og lagrer i totStr
link=$(find "$1" -depth -type f -print0 | xargs --null ls -la | awk '{ print $2 , $9 }' | sort | tail -n 1)                      #Finner hvilken fil som har flest hardlinker til seg selv og lagrer

echo "Partisjonen $1 befinner seg er $prosFull full"                                                                           #Skriver ut navn på directoriet og prosFull
echo "Det finnes $antFiler filer"                                                                                              #Skriver ut hvor mange filer som finnes i directorien
echo "Den største er $(echo "$storsteFil" | awk '{ print $2 }') som er $(echo "$storsteFil" | awk '{ print $1 }') stor."       #Skriver ut patch til største fil og hvor stor den fila er
echo "Gjennomsnittlig filstørrelse er ca $((totStr / antFiler)) bytes."                                                        #regner ut gjennomsnittlig filstørrelse og skriver ut.
echo "Filen $(echo "$link" | awk '{ print $2 }') har flest hardlinker, den har $(echo "$link" | awk '{ print $1 }')."     #Skriver ut filen med mest hardlinker til seg selv og antall hardlinker

