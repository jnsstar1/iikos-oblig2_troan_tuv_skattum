#!/bin/bash
pid=$(pgrep chrome)                                                #Lager en liste med alle prosessene som kjører på chrome
array=("$pid")                                                       #Lager en array som inneholder alle prosessnumrene.

for i in "${array[@]}"; do                                                #Går gjennom alle prosessene 
  antFaults=$(ps --no-headers -o maj_flt "$i")                            #finner antall major page faults.
  if [[ $antFaults -gt 1000 ]]; then                                      #hvis ant page faults er større enn 1000
    echo "Prosessid $i har $antFaults major page faults(mer enn 1000!)"   #Skriv melding
  else
    echo "Prosessid $i har $antFaults major page faults"                  #Ellers skriv melding
  fi
done                                                                       #Ferdig
