#!/bin/bash
# no arguments, display a menu with option for info
# about processes

clear                                        #Tøm skjerm
while [ "$svar" != "9" ]                     #Sålenge svar ikke er 9
do                                           #Skriv meny
 echo ""
 echo "  1 - Hvem er jeg?"
 echo "  2 - Tid siden siste boot"
 echo "  3 - Gjennomsnittlig load siste minutt"
 echo "  4 - Hvor mange tråder og prosesser"
 echo "  5 - Antall contextswitcher siste sekund"
 echo "  6 - Antall interrupts siste sekund"
 echo "  9 - Avslutt dette scriptet"
 echo ""
 echo -n "Velg en funksjon: "
 read -r svar                                      #Les inn svar
 echo ""
 case $svar in                                     #Switch på svar
  1)clear
    echo "Jeg er $(whoami) og scriptet heter $0"   #Finner bruker og navn på script og skriver på skjerm
    read -r                                        #les inn kommando
    clear
    ;;
  2)clear
    echo "Tid siden siste boot: $(who -b)"        #Finner tid siden siste boot og skriver på skjerm 
    read -r
    clear
    ;; 
  3)clear
    echo "Gjennomsnittlig load siste minutt: $(uptime | awk '{ print $9 }')"       #Finner gjennomsnittlig load siste minutt og skriver på skjerm
    read -r
    clear
    ;;
  4)clear
    #Finner hvor mange prosesser og tråder som finnes og skriver på skjerm
    echo "Hvor mange prosesser: $(ps aux | wc -l), og hvor mange tråder: $(ps -eo nlwp | tail -n +2 | awk '{ num_threads += $1 } END { print num_threads }')"   
    read -r
    clear
    ;;
  5)clear
    t1=$(grep ctxt /proc/stat | awk '{ print $2 }')                   #Finner antall context switcher
    sleep 1                                                           #Venter 1 sekund
    t2=$(grep ctxt /proc/stat | awk '{ print $2 }')                   #Finner antall context swither etter ventet et sekund
    echo "Antall context switcher siste sekund: $((t2-t1))"           #regner ut antall context switcher siste sekundet
    read -r
    clear
    ;;
  6)clear
    echo "Antall interrupts per sekund: $(vmstat 1 2 | tail -1 | awk '{ print $11 }')"        #Regner ut antall interrupt siste sekund og skriver på skjerm
    read -r
    clear
    ;;
  9)echo Scriptet avsluttet
    exit
    ;;
  *)echo Ugyldig valg                                                                     #Hvis valg er ugyldig 
    read -r                                                                               #Les inn nytt valg
    clear
    ;;
 esac
done

